const express = require('express');
const jwt = require('jsonwebtoken');
const authConfig = require('../config/auth')
const User = require('../models/user');
const router = express.Router();

function generateToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400
    });
}

router.post('/authenticate', async (req, res) => {
    const { name, email } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
        const user = await User.create(req.body);
        return res.send({ auth: true, token: generateToken({ id: user.id }) });
    }
    res.send({
        auth: true,
        token: generateToken({ id: user.id })
    });
});

module.exports = app => app.use('/auth', router);