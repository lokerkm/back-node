const express = require('express');
const jwt = require('jsonwebtoken');
const authConfig = require('../config/auth')
const User = require('../models/user');
const router = express.Router();
const authMiddleware = require('../middlewares/auth')
router.use(authMiddleware);

function generateToken(params = {}) {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400
    });
}

router.get('/get', async (req, res) => {
    const user = await User.findOne({_id: req.userId });
    if (!user) {
        return res.status(401).send({ error: 'User not found'});
    }
    res.status(200).send({
       user
    });
});

router.get('/getAll', async (req, res) => {
    const users = await User.find({});
    res.status(200).send({
       users
    });
});

module.exports = app => app.use('/user', router);